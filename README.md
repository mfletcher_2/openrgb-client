# OpenRGB Java CLient

A Java implementation of a OpenRGB SDK client.

Thanks to the other implementations, which helped a lot.

* https://github.com/Breina/OpenRGB.jar
* https://github.com/vlakreeh/openrgb

## Usage

See [examples folder](src/main/java/io/gitlab/mguimard/openrgb/examples)

Hosted on [maven central repository](https://mvnrepository.com/artifact/io.gitlab.mguimard/openrgb-client)
